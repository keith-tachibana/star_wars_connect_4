# Star Wars Connect 4
A Star Wars themed version of the classic Connect 4 game, but written in vanilla JavaScript, HTML, and CSS.
## Features
- _*_ Players can choose game options, such as board size and player names
- _*_ Players can keep track of game statistics such as number of games won and number of seconds left per turn
- _*_ Players must make a move within 15 seconds each turn or will otherwise lose the game
- _*_ Players can reset the game options at any time
- _*_ Players can choose to play again when either player wins or loses
