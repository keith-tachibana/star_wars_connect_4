const gameContainer = document.querySelector('.game')
var count = 0;
var loseTime;
var losingTime = document.getElementById('loseTimer').textContent;
var restartButtonLoser = document.getElementById('loseScreen');
var restartButtonWinner = document.getElementById('finish-game');
var playerOneWin = document.getElementById('player-one-wins').textContent;
var playerTwoWin = document.getElementById('player-two-wins').textContent;
var startBox = document.querySelector('.startBox')
var decision = startBox.querySelector('#decideOfSize')
var bTn = decision.querySelector('button')
var restartBtn = document.querySelector('.restartBtn')

function restartGameLoss() {
  document.getElementById('loseScreen').classList.add('hidden');
  winningPlayerAdd();
  clearAllElements();
}

function restartGameWin() {
  document.getElementById('finish-game').classList.add('hidden');
  winningPlayerAdd();
  clearAllElements();
}

function winningPlayerAdd() {
  if (count % 2 === 1) {
    playerOneWin++;
    document.getElementById('player-one-wins').textContent = playerOneWin;
  } else {
    playerTwoWin++;
    document.getElementById('player-two-wins').textContent = playerTwoWin;
  }
  count = 0;
  losingTime = 15;
  document.getElementById('loseTimer').textContent = losingTime;
}

function clearAllElements() {
  var clearElements = document.querySelectorAll('.row');
  for (var k = 0; k < clearElements.length; k++) {
    clearElements[k].textContent = 'white';
    clearElements[k].classList.remove('player-one');
    clearElements[k].classList.remove('player-two');
    clearElements[k].classList.remove('counting');
  }
  count = 0;
  losingTime = 15;
  loseTime = setInterval(loseGame, 1000);
  document.getElementById('current-player').textContent = 1;
}

function changeTurn(e) {
  var newArr = Array.from(e.target.parentElement.children)

  if (!e.target.className.includes('row')) {
    return;
  }
  if (count % 2 === 0) {
    for (let i = newArr.length; i >= 0; i--) {
      if (newArr[0].getAttribute('class').includes('counting')) {
        return;
      }
    }
    for(let i = newArr.length-1; i >= 0; i--) {
      if (!newArr[i].className.includes('player')) {
        newArr[i].classList.add('player-one', 'counting')
        newArr[i].textContent = 'red'
        document.getElementById('current-player').textContent = '2';
        losingTime = 15;
        break;
      }
    }
    count++
  } else {
    for (let i = newArr.length; i >= 0; i--) {
      if (newArr[0].getAttribute('class').includes('counting')) {
        return;
      }
    }
    for(let i = newArr.length-1; i >= 0; i--) {
      if (!newArr[i].className.includes('player')) {
        newArr[i].classList.add('player-two', 'counting')
        newArr[i].textContent = 'blue'
        document.getElementById('current-player').textContent = '1';
        losingTime = 15;
        break;
      }
    }
    count++
  }
}

function loseGame() {
  losingTime--;
  if (losingTime <= 5) {
    document.getElementById('loseTimer').classList.add('timerAlert');
  } else if (losingTime > 5) {
  document.getElementById('loseTimer').classList.remove('timerAlert');
  }
  if (losingTime === 0) {
    if (count % 2 === 0) {
      document.getElementById('losing-player').textContent = '1';
      document.getElementById('loseScreen').style.backgroundImage = 'url(./images/59264801.jpeg)'
    } else {
      document.getElementById('losing-player').textContent = '2';
      document.getElementById('loseScreen').style.backgroundImage = 'url(./images/andrew.png)'
    }
    document.getElementById('loseTimer').classList.remove('timerAlert');
    clearInterval(loseTime);
    document.getElementById('loseScreen').classList.remove('hidden');
  }
  document.getElementById('loseTimer').textContent = losingTime;
}

function decideOfSize(e) {
  e.preventDefault()
  var parentElement = Array.from(e.target.parentElement.elements)
  console.log(parentElement);
  for (let i = 0; i < parentElement.length; i++) {
    if (parentElement[i].id === "columnOfGame") {
      if (parentElement[i].value < 6 || parentElement[i].value > 10) {
        alert('Please enter a number between 6 and 10.')
        return;
      } else {
        var col = parentElement[i].value
      }
    }
    if (parentElement[i].id === "rowOfGame") {
      if (parentElement[i].value < 5 || parentElement[i].value > 9) {
        alert('Please enter a number between 5 and 9.')
        return;
      } else {
        var row = parentElement[i].value
      }66
    }
  }
  startBox.classList.add('hidden')
  document.querySelector('aside').classList.remove('hidden');
  gameContainer.classList.remove('hidden');
  loseTime = setInterval(loseGame, 1000);
  structure(col, row);
}

function structure(colL, rowL) {
  for (let i = 0; i < colL; i++) {
    var colDiv = document.createElement('div')
    colDiv.classList.add('col', `${i}`)
    for (let j = 0; j < rowL; j++) {
      var rowDiv = document.createElement('div')
      rowDiv.classList.add('row', `${j}`)
      rowDiv.textContent = 'white';
      colDiv.prepend(rowDiv)
    }
  gameContainer.append(colDiv)
  }
}

function refreshAll() {
  location.reload()
}

function init() {
  bTn.addEventListener('click', decideOfSize);
  gameContainer.addEventListener('click', changeTurn)
  restartButtonLoser.addEventListener('click', restartGameLoss);
  restartButtonWinner.addEventListener('click', restartGameWin);
  restartBtn.addEventListener('click', refreshAll)
  structure();
}

init();
