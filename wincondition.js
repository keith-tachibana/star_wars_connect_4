var player1 = "red"
var player2 = "blue"

function playerOneWins() {
  document.getElementById('winning-player').textContent = '1';
  document.getElementById('finish-game').classList.remove('hidden');
  document.getElementById('finish-game').style.backgroundImage = 'url(./images/59264801.jpeg)'
}

function playerTwoWins() {
  document.getElementById('winning-player').textContent = '2';
  document.getElementById('finish-game').classList.remove('hidden');
  document.getElementById('finish-game').style.backgroundImage = 'url(./images/andrew.png)'
}

function horizon(arr, lngt) {
  for (var i = 0; i < arr.length; i++) {
    if (arr[i] === player1) {
      if (arr[i + lngt] === player1) {
        if (arr[i + (lngt * 2)] === player1) {
          if (arr[i + (lngt * 3)] === player1) {
            playerOneWins();
            clearInterval(loseTime);
          }
        }
      }
    }
  }
    if (arr[i] === player2) {
      if (arr[i + lngt] === player2) {
        if (arr[i + (lngt * 2)] === player2) {
          if (arr[i + (lngt * 3)] === player2) {
            playerTwoWins();
            clearInterval(loseTime);
          }
        }
      }
    }
  }


function vertical(arr, lngt, col) {
  for (let i = 0; i < arr.length; i++) {
    for (let j = 0; j < col; j++) {
      if (arr[i] === player1 && ((lngt * j) <= i && i < lngt * (j + 1))) {
        if (arr[i + 1] === player1 && ((lngt * j) <= i + 1 && i + 1 < lngt * (j + 1))) {
          if (arr[i + 2] === player1 && ((lngt * j) <= i + 2 && i + 2 < lngt * (j + 1))) {
            if (arr[i + 3] === player1 && ((lngt * j) <= i + 3 && i + 3 < lngt * (j + 1))) {
              playerOneWins();
              clearInterval(loseTime);
            }
          }
        }
      }
      if (arr[i] === player2 && ((lngt * j) <= i && i < lngt * (j + 1))) {
        if (arr[i + 1] === player2 && ((lngt * j) <= i + 1 && i + 1 < lngt * (j + 1))) {
          if (arr[i + 2] === player2 && ((lngt * j) <= i + 2 && i + 2 < lngt * (j + 1))) {
            if (arr[i + 3] === player2 && ((lngt * j) <= i + 3 && i + 3 < lngt * (j + 1))) {
              playerTwoWins();
              clearInterval(loseTime);
            }
          }
        }
      }
    }
  }
}

function diagonal(arr, lngt, col) {
  for (let i = 0; i < arr.length; i++) {
    for (let j = 0; j < col; j++) {
      if (arr[i + lngt * (j)] === player1 && ((lngt * j) <= i + lngt * (j) && i + lngt * (j) < lngt * (j + 1) * 1 - 3)) {
        if (arr[i + (lngt * (j + 1)) + 1] === player1 && ((lngt * (j + 1)) <= i + (lngt * (j + 1)) + 1 && i + (lngt * (j + 1)) + 1 < lngt * ((j + 1) * 2))) {
          if (arr[i + (lngt * (j + 2)) + 2] === player1 && ((lngt * (j + 2)) <= i + (lngt * (j + 2)) + 2 && i + (lngt * (j + 2)) + 2 < lngt * ((j + 1) * 3))) {
            if (arr[i + (lngt * (j + 3)) + 3] === player1 && ((lngt * (j + 3)) <= i + (lngt * (j + 3)) + 3 && i + (lngt * (j + 3)) + 3 < lngt * ((j + 1) * 4))) {
              playerOneWins();
              clearInterval(loseTime);
            }
          }
        }
      }
      if (arr[i + lngt * (j)] === player1 && ((lngt * j) + 2 <= i + lngt * (j) && i + lngt * (j) < lngt * (j + 1) * 1)) {
        if (arr[i + (lngt * (j + 1)) - 1] === player1 && ((lngt * (j + 1)) <= i + (lngt * (j + 1)) - 1 && i + (lngt * (j + 1)) - 1 < lngt * ((j + 1) * 2))) {
          if (arr[i + (lngt * (j + 2)) - 2] === player1 && ((lngt * (j + 2)) <= i + (lngt * (j + 2)) - 2 && i + (lngt * (j + 2)) - 2 < lngt * ((j + 1) * 3))) {
            if (arr[i + (lngt * (j + 3)) - 3] === player1 && ((lngt * (j + 3)) <= i + (lngt * (j + 3)) - 3 && i + (lngt * (j + 3)) - 3 < lngt * ((j + 1) * 4))) {
              playerOneWins();
              clearInterval(loseTime);
            }
          }
        }
      }
      if (arr[i + lngt * (j)] === player2 && ((lngt * j) <= i + lngt * (j) && i + lngt * (j) < lngt * (j + 1) * 1 - 3)) {
        if (arr[i + (lngt * (j + 1)) + 1] === player2 && ((lngt * (j + 1)) <= i + (lngt * (j + 1)) + 1 && i + (lngt * (j + 1)) + 1 < lngt * ((j + 1) * 2))) {
          if (arr[i + (lngt * (j + 2)) + 2] === player2 && ((lngt * (j + 2)) <= i + (lngt * (j + 2)) + 2 && i + (lngt * (j + 2)) + 2 < lngt * ((j + 1) * 3))) {
            if (arr[i + (lngt * (j + 3)) + 3] === player2 && ((lngt * (j + 3)) <= i + (lngt * (j + 3)) + 3 && i + (lngt * (j + 3)) + 3 < lngt * ((j + 1) * 4))) {
              playerTwoWins();
              clearInterval(loseTime);
            }
          }
        }
      }
      if (arr[i + lngt * (j)] === player2 && ((lngt * j) + 2 <= i + lngt * (j) && i + lngt * (j) < lngt * (j + 1) * 1)) {
        if (arr[i + (lngt * (j + 1)) - 1] === player2 && ((lngt * (j + 1)) <= i + (lngt * (j + 1)) - 1 && i + (lngt * (j + 1)) - 1 < lngt * ((j + 1) * 2))) {
          if (arr[i + (lngt * (j + 2)) - 2] === player2 && ((lngt * (j + 2)) <= i + (lngt * (j + 2)) - 2 && i + (lngt * (j + 2)) - 2 < lngt * ((j + 1) * 3))) {
            if (arr[i + (lngt * (j + 3)) - 3] === player2 && ((lngt * (j + 3)) <= i + (lngt * (j + 3)) - 3 && i + (lngt * (j + 3)) - 3 < lngt * ((j + 1) * 4))) {
              playerTwoWins();
              clearInterval(loseTime);
            }
          }
        }
      }
    }
  }
}

function checkWinCondition() {
  var col = document.querySelectorAll('.col')
  var checkArr = []
  var parentArray = Array.from(col)
  for (let i = 0; i < parentArray.length; i++) {
    var childrenArray = Array.from(parentArray[i].children)
    for (let j = childrenArray.length-1; j >= 0; j--) {
      checkArr.push(childrenArray[j].textContent)
    }
  }
  vertical(checkArr, childrenArray.length, parentArray.length);
  horizon(checkArr, childrenArray.length);
  diagonal(checkArr, childrenArray.length, parentArray.length);
}


function init() {
  gameContainer.addEventListener('click', checkWinCondition)
}

init()
