const timeContainer = document.querySelector('.timeZone')

function getTime() {
  const data = new Date();
  var hr = data.getHours()
  var min = data.getMinutes()
  var sec = data.getSeconds()
  timeContainer.innerHTML = `${hr < 10 ? `0${hr}` : hr}: ${min < 10 ? `0${min}` : min}: ${sec < 10 ? `0${sec}` : sec}`
}

function init() {
  setInterval(getTime, 1000)
}

init()
